///
/// Program-Name:   Discord to HTML Converter
/// Description:    generated a HTML file out of a copied discord file
/// Author:         ConfusedAnt (Yanik Ammann)
/// Prerequisites:  none
/// Build-command:  rm main; valac --pkg gtk+-3.0 main.vala; ./main
/// Git-Repository: https://gitlab.com/ConfusedAnt/
///
/// To-Do:          - add support for usernames with numbers (##/)
///                 - fix detection of mobile
///
/// Versions (version-number, date, author, description):
/// v0.1, 14.12.2019, ConfusedAnt, starting developement
/// v0.2, 15.12.2019, ConfusedAnt, fixed some stuff
/// v0.3, 14.12.2019, ConfusedAnt, set content to mobile optimized
///
/// License:        This program is free software: you can redistribute it and/or modify
///                 it under the terms of the GNU General Public License as published by
///                 the Free Software Foundation, either version 3 of the License, or
///                 (at your option) any later version.
///
///                 You should have received a copy of the GNU General Public License
///                 along with this program.  If not, see <http://www.gnu.org/licenses/>.
///

using Gtk;

class RandomBoyTask : Window {
  TextView text_view;
  string beginningOfMessage = "    
    <div class='chat'>
      <table>
        <tr><td>";
  string exportBeginning = "<html>
          <head>
            <title>For the best boyo ever</title>
            <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato'></link>
            <script src='https://kit.fontawesome.com/2c4aca1332.js' crossorigin='anonymous'></script>
            <style>
              * {
                font-family: 'Lato', sans-serif;
              }
              
              body {
                margin: 15px;
                background-color: #2C3133;
                color: #f5f5f5;
              }
              
              h1 {
                font-size: 1.8em;
                text-align: center;
                padding: 10px;
                margin: -20px;
                background-color: #202020;
                margin-bottom: 10px;
              }
              
              .chat {
                background-color: #202020;
                width: 400px;
                border-radius: 17px;
                padding: 5px;
                margin: 10px;
                float: left;
              }
              
              .chat img {
                vertical-align: top;
                margin: 3px;
                border-radius: 15px;
              }
              
              .full {
                height: 100%;
                width: 100%;
              }
              
              .date {
                vertical-align: top;
              }
              
              .date td:last-child {
                text-align: end;
                padding-right: 5px;
              }
              
              .text {
                vertical-align: initial;
              }
              
              @media (orientation: portrait) {
					.chat {
		                display: contents !important;
		            }
				}
		      .chat {
                 display: contents !important;
               }
            </style>
          </head>
          <body>
            <h1>Chat history with Kai Lee <i class='fas fa-heart'></i></h1>";
  
	RandomBoyTask () {
		// set header parameters
		var header = new HeaderBar ();
		header.title = "Application Title";
		header.show_close_button = true;
		// set window parameters
		window_position = WindowPosition.CENTER;
		set_titlebar(header);
		set_default_size (500, 300);
		border_width = 2;
		
		// define buttons
		Gtk.Button open_button = new Gtk.Button.from_icon_name ("document-open");
        open_button.clicked.connect (on_open_clicked);
        
		Gtk.Button save_button = new Gtk.Button.from_icon_name ("document-save");
        save_button.clicked.connect (on_save_clicked);
        
		Gtk.Button export_button = new Gtk.Button.from_icon_name ("document-export");
        export_button.clicked.connect (on_export_clicked);
        
        // add buttons to header
        header.add(open_button);
        header.add(save_button);
        header.pack_end(export_button);        
        
        // add textView
        text_view = new TextView ();
        text_view.wrap_mode = WORD;
        
        // create window that's scrollable and has textView in it
        var scroll = new ScrolledWindow (null, null);
        scroll.set_policy (PolicyType.AUTOMATIC, PolicyType.AUTOMATIC);
        scroll.add (text_view);
        var vbox = new Box (Orientation.VERTICAL, 0);
        vbox.pack_start (scroll, true, true, 0);
        add (vbox);
        
	}
	
	private void on_export_clicked () {
	    var file_chooser = new FileChooserDialog ("Save File", this,
                                      FileChooserAction.SAVE,
                                      "_Cancel", ResponseType.CANCEL,
                                      "_Save", ResponseType.ACCEPT);
        if (file_chooser.run () == ResponseType.ACCEPT) {
            export_file(file_chooser.get_filename ());
        }
        file_chooser.destroy ();
	}
	private void export_file(string filename) {
        string output = exportBeginning;
    
    // split by message
    string[] message = this.text_view.buffer.text.split("\n \n");
    
    // for every message
    foreach (string messageString in message) {
		output += beginningOfMessage;
		string[] messageArray = messageString.split("\n", 2); // header and content
		
		Regex usernameRegex = new Regex ("^[^0-9]*");
		Regex dateRegex = new Regex ("[0-9].*");
		
		string date = usernameRegex.replace (messageArray[0], messageArray[0].length, 0, "");
		string username = dateRegex.replace (messageArray[0], messageArray[0].length, 0, "");
		string content = messageArray[1];
		
		switch (username) {
		    case "SteamPoweredKoalaBear":
		        username = "Kai Lee";
		        break;
		    case "ConfusedAnt":
		    case "SteamPoweredVirtualAnt":
		        username = "Yanik Ammann";
		        break;
		    default:
		        break;
		}
		
		content = content.replace(":sparkling_heart:", "<i class='fas fa-heart'></i>");
		content = content.replace(":joy:", "<i class='fas fa-grin-tears'></i>");
		content = content.replace(":heart_eyes:", "<i class='fas fa-grin-hearts'></i>");
		content = content.replace(":sob:", "<i class='fas fa-sad-cry'></i>");
		content = content.replace(":sweat_smile:", "<i class='fas fa-grin-beam-sweat'></i>");
		
		output += "<img src='" + username + ".png'/></td><td class='full'><table class='full'><tr class='date'><td>" + username + "</td><td>" + date + "</td></tr><tr><td class='text' colspan='2'>" + content + "</table></td>
        </tr>
      </table>
    </div>";
	}
    
    output += "  </body>
        </html>";
    
        //save file
        try {
            FileUtils.set_contents (filename + ".html", output);
        } catch (Error e) {
            stderr.printf ("Error: %s\n", e.message);
        }
    }
	
	private void on_open_clicked () {
        var file_chooser = new FileChooserDialog ("Open File", this,
                                      FileChooserAction.OPEN,
                                      "_Cancel", ResponseType.CANCEL,
                                      "_Open", ResponseType.ACCEPT);
        if (file_chooser.run () == ResponseType.ACCEPT) {
            open_file (file_chooser.get_filename ());
        }
        file_chooser.destroy ();
    }
    private void open_file (string filename) {
        try {
            string text;
            FileUtils.get_contents (filename, out text);
            this.text_view.buffer.text = text;
        } catch (Error e) {
            stderr.printf ("Error: %s\n", e.message);
        }
    }
    
    private void on_save_clicked () {
        var file_chooser = new FileChooserDialog ("Save File", this,
                                      FileChooserAction.SAVE,
                                      "_Cancel", ResponseType.CANCEL,
                                      "_Save", ResponseType.ACCEPT);
        if (file_chooser.run () == ResponseType.ACCEPT) {
            save_file (file_chooser.get_filename ());
        }
        file_chooser.destroy ();
    }
    private void save_file (string filename) {
        try {
            string text;
            FileUtils.set_contents (filename, this.text_view.buffer.text);
        } catch (Error e) {
            stderr.printf ("Error: %s\n", e.message);
        }
    }
			
	static int main (string[] args) {
		init (ref args);

		// create window
		var window = new RandomBoyTask();
		window.destroy.connect(main_quit);
		window.show_all();

		Gtk.main ();
		return 0;
	}
}
